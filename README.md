# WeekSelector

##### 1. 引用

```javascript
<script src="http://res.wisedu.com/fe_components/jquery-1.11.3.js"></script>
<link rel="stylesheet" type="text/css" href="./weekSelector.css" />
<script src="./weekSelector.js"></script>
```



##### 2. html

```html
<table style="margin-top:10px">
    <tr>
    	<td style="padding:10px">周次</td>
        <td><div class="d"></div></td>
    </tr>
</table>
```



##### 3. js

```javascript
var w = new WeekSelector({
    container: ".d",
    weekNum: '101010100000000000000000000000'
    //,weekName: '1-7单周'
});
w.render();

function getVal() {
    var obj = w.getResult();
    console.log(obj);
    alert(obj.weekNum + '\n' + obj.weekName);
}
```

##### 4. 参数说明

| 参数名           | 说明                                       |
| ------------- | ---------------------------------------- |
| **container** | 必须，生成的二维表将会显示在指定的container中。container写法与jquery选择器写法一致，如：#containerId、.className等 |
| weekTotal     | 非必须，周次总数，默认30                            |
| weekNum       | 非必须，设置周次的二进制串（00010000）                  |